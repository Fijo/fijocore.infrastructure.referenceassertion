using System;
using System.Reflection;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;

namespace FijoCore.Infrastructure.ReferenceAssertion.Dtos {
	[Serializable]
	public class ReferenceAssertition {
		public readonly Assembly Context;
		public readonly ReferenceAssertitionVerifier ReferenceAssertitionVerifier;
		public readonly object OperationOrProperty;
		public readonly object Obj;
		public readonly Func<object, bool> Custom;

		public ReferenceAssertition(Assembly context, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, object obj) {
			Context = context;
			ReferenceAssertitionVerifier = referenceAssertitionVerifier;
			OperationOrProperty = operationOrProperty;
			Obj = obj;
		}

		public ReferenceAssertition(Assembly context, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, Func<object, bool> custom) {
			Context = context;
			ReferenceAssertitionVerifier = referenceAssertitionVerifier;
			OperationOrProperty = operationOrProperty;
			Custom = custom;
		}
	}
}