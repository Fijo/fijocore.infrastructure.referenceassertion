using System;

namespace FijoCore.Infrastructure.ReferenceAssertion.Enums {
	[Serializable]
	public enum ReferenceAssertitionVerifier {
		Language,
		Runtime,
		Version,
		DefinedContstants
	}
}