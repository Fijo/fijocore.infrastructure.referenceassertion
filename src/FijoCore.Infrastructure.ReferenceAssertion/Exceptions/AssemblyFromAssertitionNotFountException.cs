using System;

namespace FijoCore.Infrastructure.ReferenceAssertion.Exceptions {
	[Serializable]
	public class AssemblyFromAssertitionNotFountException : Exception {
		public AssemblyFromAssertitionNotFountException() {}
		public AssemblyFromAssertitionNotFountException(string name) : base(name) {}
		public AssemblyFromAssertitionNotFountException(string name, Exception innerException) : base(name, innerException) {}
	}
}