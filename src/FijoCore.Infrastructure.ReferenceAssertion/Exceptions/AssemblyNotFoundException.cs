using System;

namespace FijoCore.Infrastructure.ReferenceAssertion.Exceptions {
	[Serializable]
	public class AssemblyNotFoundException : Exception {
		public AssemblyNotFoundException() {}
		public AssemblyNotFoundException(string assemblyName) : base(assemblyName) {}
		public AssemblyNotFoundException(string assemblyName, Exception innerException) : base(assemblyName, innerException) {}
	}
}