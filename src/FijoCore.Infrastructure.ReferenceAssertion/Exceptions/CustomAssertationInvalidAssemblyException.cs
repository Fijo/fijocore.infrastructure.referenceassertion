using System;
using System.Reflection;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;

namespace FijoCore.Infrastructure.ReferenceAssertion.Exceptions {
	[Serializable]
	public class CustomAssertationInvalidAssemblyException : InvalidAssemblyException {
		public CustomAssertationInvalidAssemblyException() {}
		public CustomAssertationInvalidAssemblyException(Assembly context, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, string custom, Exception innerException = null)
			: base(string.Format("A custom assertation �{0}� faild for �{1}�. The �x� argument had been resolved using �{2}� with �{3}�.", custom, context.GetName().Name, Enum.GetName(typeof (ReferenceAssertitionVerifier), referenceAssertitionVerifier), operationOrProperty), innerException) {}
	}
}