using System;
using System.Reflection;

namespace FijoCore.Infrastructure.ReferenceAssertion.Exceptions {
	[Serializable]
	public class InvalidAssemblyException : Exception {
		public InvalidAssemblyException() {}
		public InvalidAssemblyException(string message) : base(message) {}
		public InvalidAssemblyException(string message, Exception innerException) : base(message, innerException) {}
		public InvalidAssemblyException(Assembly contextAssembly, string message, Exception innerException = null) : base(string.Format("Assertation faild for �{0}�. {1}", contextAssembly.GetName().Name, message), innerException) {}
	}
}