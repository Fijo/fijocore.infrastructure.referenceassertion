using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using FijoCore.Infrastructure.ReferenceAssertion.Services;

namespace FijoCore.Infrastructure.ReferenceAssertion.Factories {
	public class ReferenceAssertitionFactory {
		private readonly AssembliesProvider _assembliesProvider = Kernel.Resolve<AssembliesProvider>();

		public ReferenceAssertitionProvider CreateProvider() {
			return new ReferenceAssertitionProvider(_assembliesProvider.GetAssemblies());
		}
	}
}