using System;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.ReferenceAssertion.Services;
using JetBrains.Annotations;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.ReferenceAssertion.Module {
	[PublicAPI]
	public class ReferenceAssertionInit : IExtendedInitHandler {
		#region Implementation of IExtendedInitHandler
		public void Handle(IKernel kernel, IExtendedNinjectModule module, object obj) {
			#region PreCondition
			Debug.Assert(obj is Action<ReferenceAssertitionProvider>);
			#endregion
			var provider = Kernel.Resolve<ReferenceAssertitionProvider>();
			provider.SetContextAssembly(module.GetType());
		}
		#endregion
	}
}