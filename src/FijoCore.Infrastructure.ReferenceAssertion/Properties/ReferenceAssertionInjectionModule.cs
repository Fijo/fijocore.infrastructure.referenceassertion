﻿using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Factories;
using FijoCore.Infrastructure.ReferenceAssertion.Module;
using FijoCore.Infrastructure.ReferenceAssertion.Repositories;
using FijoCore.Infrastructure.ReferenceAssertion.Services;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Interface;
using Ninject;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.ReferenceAssertion.Properties
{
    public class ReferenceAssertionInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

    	public override void OnLoad(IKernel kernel) {
    		kernel.Bind<DefinedContstantsVerifier>().ToSelf().InSingletonScope();
    		kernel.Bind<LanguageVerifier>().ToSelf().InSingletonScope();
    		kernel.Bind<RuntimeVerifier>().ToSelf().InSingletonScope();
    		kernel.Bind<VersionVerifier>().ToSelf().InSingletonScope();

    		kernel.Bind<ReferenceAssertitionFactory>().ToSelf().InSingletonScope().OnActivation(x => Kernel.Bind<ReferenceAssertitionProvider>().ToConstant(x.CreateProvider()).InSingletonScope());
    		kernel.Bind<ReferenceAssertitionVerifierRepository>().ToSelf().InSingletonScope().OnActivation(
    			x => x.Init(new IReferenceAssertitionVerifier[]
    			{
    				Kernel.Get<DefinedContstantsVerifier>(), Kernel.Get<LanguageVerifier>(), Kernel.Get<RuntimeVerifier>(), Kernel.Get<VersionVerifier>()
    			}));
    		kernel.Bind<ReferenceAssertitionVerifierService>().ToSelf().InSingletonScope();
    		kernel.Bind<ReferenceAssertionInit>().ToSelf().InSingletonScope();
    	}

	    public override void Init(IKernel kernel) {
    		kernel.Get<ReferenceAssertitionFactory>();
    	}
		
    	public override void RegisterHandlers(IKernel kernel) {
			ExtendedInit = new Dictionary<IExtendedInitHandler, object>
			               {
			               	{kernel.Get<ReferenceAssertionInit>(), (Action<ReferenceAssertitionProvider>)(x => 
								x.Assert("FijoCore.Infrastructure.DependencyInjection", ReferenceAssertitionVerifier.DefinedContstants, "ReInject", false))}
			               };
    	}

    	public override void Loaded(IKernel kernel) {
    		kernel.Get<ReferenceAssertitionVerifierService>().Verify(kernel.Get<ReferenceAssertitionProvider>().GetAssertitions());
    	}
    }
}