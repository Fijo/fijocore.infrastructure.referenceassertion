using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.ReferenceAssertion.Repositories {
	public class ReferenceAssertitionVerifierRepository {
		private IDictionary<ReferenceAssertitionVerifier, IReferenceAssertitionVerifier> _referenceAssertitionVerifiers;

		public void Init(IEnumerable<IReferenceAssertitionVerifier> referenceAssertitionVerifiers) {
			_referenceAssertitionVerifiers = referenceAssertitionVerifiers.ToDictionary(x => x.ReferenceAssertitionVerifier, x => x);
		}

		[NotNull]
		public IReferenceAssertitionVerifier Get(ReferenceAssertitionVerifier referenceAssertitionVerifier) {
			#region PreCondition
			Debug.Assert(_referenceAssertitionVerifiers.ContainsKey(referenceAssertitionVerifier), "not supported ´referenceAssertitionVerifier´");
			#endregion
			return _referenceAssertitionVerifiers[referenceAssertitionVerifier];
		}
	}
}