using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.ReferenceAssertion.Dtos;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.ReferenceAssertion.Services {
	public class ReferenceAssertitionProvider {
		private readonly IList<ReferenceAssertition> _referenceAssertitions = new List<ReferenceAssertition>();
		private readonly IDictionary<string, Assembly> _assemblies;
		private Assembly _contextAssembly;

		public ReferenceAssertitionProvider([NotNull] IEnumerable<Assembly> assemblies) {
			_assemblies = assemblies.ToDictionary(x => x.GetName().Name, x => x);
		}
		
		public void SetContextAssembly([NotNull] Assembly contextAssembly) {
			_contextAssembly = contextAssembly;
		}

		public void SetContextAssembly(string contextAssembly) {
			try {
				_contextAssembly = GetAssemblyByName(contextAssembly);
			}
			catch (AssemblyFromAssertitionNotFountException) {
				throw new AssemblyNotFoundException(contextAssembly);
			}
		}
		
		public void SetContextAssembly([NotNull] object contextAssembly) {
			SetContextAssembly(contextAssembly.GetType());
		}

		public void SetContextAssembly([NotNull] Type contextAssembly) {
			try {
				_contextAssembly = GetAssemblyByTypes(contextAssembly);
			}
			catch (AssemblyFromAssertitionNotFountException) {
				throw new AssemblyNotFoundException(GetAssemblyNotFoundByTypeMessage(contextAssembly));
			}
		}

		[NotNull, Pure]
		public IEnumerable<ReferenceAssertition> GetAssertitions() {
			return _referenceAssertitions;
		}

		public void Assert(ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, object obj) {
			AddAssertition(_contextAssembly, referenceAssertitionVerifier, operationOrProperty, obj);
		}

		public void Assert(Assembly contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, object obj) {
			AddAssertition(contextAssembly, referenceAssertitionVerifier, operationOrProperty, obj);
		}

		public void Assert(string contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, object obj) {
			AddAssertition(GetAssemblyByName(contextAssembly), referenceAssertitionVerifier, operationOrProperty, obj);
		}

		public void Assert(Type contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, object obj) {
			AddAssertition(GetAssemblyByTypes(contextAssembly), referenceAssertitionVerifier, operationOrProperty, obj);
		}

		public void Assert(ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, Func<object, bool> custom) {
			AddAssertition(_contextAssembly, referenceAssertitionVerifier, operationOrProperty, custom);
		}

		public void Assert(Assembly contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, Func<object, bool> custom) {
			AddAssertition(contextAssembly, referenceAssertitionVerifier, operationOrProperty, custom);
		}

		public void Assert(string contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, Func<object, bool> custom) {
			AddAssertition(GetAssemblyByName(contextAssembly), referenceAssertitionVerifier, operationOrProperty, custom);
		}

		public void Assert(Type contextAssembly, ReferenceAssertitionVerifier referenceAssertitionVerifier, object operationOrProperty, Func<object, bool> custom) {
			AddAssertition(GetAssemblyByTypes(contextAssembly), referenceAssertitionVerifier, operationOrProperty, custom);
		}

		private Assembly GetAssemblyByName(string name) {
			Assembly value;
			if(!_assemblies.TryGetValue(name, out value)) throw new AssemblyFromAssertitionNotFountException(name);
			return value;
		}

		private Assembly GetAssemblyByTypes([NotNull] Type type) {
			var result = _assemblies.Values.FirstOrDefault(x => x.GetTypes().Contains(type));
			if(result == null) throw new AssemblyFromAssertitionNotFountException(GetAssemblyNotFoundByTypeMessage(type));
			return result;
		}

		private string GetAssemblyNotFoundByTypeMessage(Type type) {
			return string.Format("FATAL! Assembly not found by type �{0}�.", type.Name);
		}

		private void AddAssertition([NotNull] Assembly context, ReferenceAssertitionVerifier referenceAssertitionVerifier, [NotNull] object operationOrProperty, [NotNull] object obj) {
			_referenceAssertitions.Add(new ReferenceAssertition(context, referenceAssertitionVerifier, operationOrProperty, obj));
		}

		private void AddAssertition([NotNull] Assembly context, ReferenceAssertitionVerifier referenceAssertitionVerifier, [NotNull] object operationOrProperty, [NotNull] Func<object, bool> custom) {
			_referenceAssertitions.Add(new ReferenceAssertition(context, referenceAssertitionVerifier, operationOrProperty, custom));
		}
	}
}