using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.ReferenceAssertion.Dtos;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Repositories;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Interface;

namespace FijoCore.Infrastructure.ReferenceAssertion.Services {
	public class ReferenceAssertitionVerifierService {
		private readonly ReferenceAssertitionVerifierRepository _referenceAssertitionVerifierRepository = Kernel.Resolve<ReferenceAssertitionVerifierRepository>();

		public void Verify(IEnumerable<ReferenceAssertition> referenceAssertitions) {
			#region PreCondition
			Debug.Assert(referenceAssertitions != null, "referenceAssertitions != null");
			#endregion
			foreach (var referenceAssertition in referenceAssertitions) Verify(referenceAssertition);
		}

		public void Verify(ReferenceAssertition referenceAssertition) {
			#region PreCondition
			Debug.Assert(referenceAssertition != null, "referenceAssertition != null");
			#endregion
			var referenceAssertitionVerifier = _referenceAssertitionVerifierRepository.Get(referenceAssertition.ReferenceAssertitionVerifier);
			if(GetUseCustomFunc(referenceAssertition)) VerifyCustom(referenceAssertition, referenceAssertitionVerifier);
			else VerifyDefault(referenceAssertition, referenceAssertitionVerifier);
		}

		private static bool GetUseCustomFunc(ReferenceAssertition referenceAssertition) {
			return referenceAssertition.Custom != null;
		}

		private void VerifyDefault(ReferenceAssertition referenceAssertition, IReferenceAssertitionVerifier referenceAssertitionVerifier) {
			referenceAssertitionVerifier.Verify(referenceAssertition.Context, referenceAssertition.OperationOrProperty, referenceAssertition.Obj);
		}

		private void VerifyCustom(ReferenceAssertition referenceAssertition, IReferenceAssertitionVerifier referenceAssertitionVerifier) {
			if (!referenceAssertition.Custom(referenceAssertitionVerifier.GetProperty(referenceAssertition.Context, referenceAssertition.OperationOrProperty)))
				throw new CustomAssertationInvalidAssemblyException(referenceAssertition.Context, referenceAssertition.ReferenceAssertitionVerifier,
				                                                    referenceAssertition.OperationOrProperty, referenceAssertition.Custom.ToString());
		}
	}
}