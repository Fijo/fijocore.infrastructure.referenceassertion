using System.Diagnostics;
using System.Reflection;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Interface;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier.Base {
	public abstract class ReferenceAssertitionVerifierBase<TOperation, TObj, TProperty> : IReferenceAssertitionVerifier<TOperation, TObj, TProperty> {
		#region Implementation of IReferenceAssertitionVerifier<TOperation,TObj,TProperty>
		public abstract void Verify(Assembly context, TOperation operationOrProperty, TObj obj);
		public abstract TProperty GetProperty(Assembly context, TOperation operationOrProperty = default(TOperation));

		public void Verify(Assembly context, object operationOrProperty, object obj) {
			#region PreCondition
			Debug.Assert(context != null, "context != null");
			Debug.Assert(operationOrProperty is TOperation, string.Format("operationOrProperty should be a �{0}�", typeof(TOperation).Name));
			Debug.Assert(obj is TObj, string.Format("obj should be a �{0}�", typeof(TObj).Name));
			#endregion
			Verify(context, (TOperation) operationOrProperty, (TObj) obj);
		}

		public object GetProperty(Assembly context, object operationOrProperty = null) {
			#region PreCondition
			Debug.Assert(context != null, "context != null");
			Debug.Assert(operationOrProperty is TOperation, string.Format("operationOrProperty should be a �{0}�", typeof(TOperation).Name));
			#endregion
			return GetProperty(context, (TOperation) operationOrProperty);
		}

		public abstract ReferenceAssertitionVerifier ReferenceAssertitionVerifier { get; }
		#endregion
	}
}