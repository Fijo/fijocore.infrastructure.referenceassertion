using System;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.AssemblyConfiguration;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Base;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier {
	public class DefinedContstantsVerifier : ReferenceAssertitionVerifierBase<string, bool, bool> {
		private readonly AssemblyEnabledDefenitionResolver _assemblyEnabledDefenitionResolver = Kernel.Resolve<AssemblyEnabledDefenitionResolver>();

		#region Implementation of IReferenceAssertitionVerifier<string,bool,bool>
		public override void Verify(Assembly context, string constant, bool value) {
			if(GetProperty(context, constant) != value) throw new InvalidAssemblyException(context, string.Format("The Assembly defenition ´{0}´ constant is not set to the asserted value. Please recompile this assembly using having the ´{0}´ {1}.", constant, value ? "enabled" : "disabled"));
		}

		[Desc("you have to set ´constant´ to a value that is not null")]
		public override bool GetProperty(Assembly context, string constant = default(string)) {
			#region PreCondition
			if (string.IsNullOrEmpty(constant)) throw new ArgumentNullException("constant");
			#endregion
			return _assemblyEnabledDefenitionResolver.Get(context).Contains(constant);
		}

		public override ReferenceAssertitionVerifier ReferenceAssertitionVerifier {
			get { return ReferenceAssertitionVerifier.DefinedContstants; }
		}
		#endregion
	}
}