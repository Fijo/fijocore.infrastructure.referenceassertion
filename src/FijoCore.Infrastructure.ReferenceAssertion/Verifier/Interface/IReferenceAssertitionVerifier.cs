using System.Reflection;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier.Interface {
	public interface IReferenceAssertitionVerifier<in TOperation, in TObj, out TProperty> : IReferenceAssertitionVerifier {
		void Verify([NotNull] Assembly context, TOperation operationOrProperty, TObj obj);
		TProperty GetProperty([NotNull] Assembly context, TOperation operationOrProperty = default(TOperation));
	}

	public interface IReferenceAssertitionVerifier {
		void Verify([NotNull] Assembly context, object operationOrProperty, object obj);
		object GetProperty([NotNull] Assembly context, object operationOrProperty = default(object));
		ReferenceAssertitionVerifier ReferenceAssertitionVerifier { get; }
	}
}