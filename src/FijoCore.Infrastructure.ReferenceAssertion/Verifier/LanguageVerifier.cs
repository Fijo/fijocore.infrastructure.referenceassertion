using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Base;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier {
	public class LanguageVerifier : ReferenceAssertitionVerifierBase<RelationOperation, CultureInfo[], CultureInfo> {
		private readonly RelativeOperationComparingEnumerableRightService _relativeOperationComparingEnumerableRightService = Kernel.Resolve<RelativeOperationComparingEnumerableRightService>();

		#region Implementation of IReferenceAssertitionVerifier<RelationOperation,CultureInfo[],CultureInfo>
		public override void Verify(Assembly context, RelationOperation relationOperation, CultureInfo[] objects) {
			if(!_relativeOperationComparingEnumerableRightService.Compare(relationOperation, GetProperty(context, default(RelationOperation)), objects)) throw new InvalidAssemblyException(context, string.Format("Language does not match the assertation. It should be �{0}� �{1}�.", Enum.GetName(typeof(RelationOperation), relationOperation), string.Join(", ", objects.Select(x => x.EnglishName))));
		}

		public override CultureInfo GetProperty(Assembly context, RelationOperation relationOperation = default(RelationOperation)) {
			return context.GetName().CultureInfo;
		}

		public override ReferenceAssertitionVerifier ReferenceAssertitionVerifier {
			get { return ReferenceAssertitionVerifier.Language; }
		}
		#endregion
	}
}