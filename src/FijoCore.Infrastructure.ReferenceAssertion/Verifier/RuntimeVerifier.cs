using System;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Module;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Base;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier {
	public class RuntimeVerifier : ReferenceAssertitionVerifierBase<RelationOperation, Runtime[], Runtime> {
		private readonly RelativeOperationComparingEnumerableRightService _relativeOperationComparingEnumerableRightService = Kernel.Resolve<RelativeOperationComparingEnumerableRightService>();
		private readonly RuntimeConverter _runtimeConverter = Kernel.Resolve<RuntimeConverter>();

		#region Implementation of IReferenceAssertitionVerifier<RelationOperation,Runtime[],Runtime>
		public override void Verify(Assembly context, RelationOperation relationOperation, Runtime[] objects) {
			if(!_relativeOperationComparingEnumerableRightService.Compare(relationOperation, GetProperty(context, default(RelationOperation)), objects)) throw new InvalidAssemblyException(context, string.Format("Runtime does not match the assertation. It should be �{0}� �{1}�.", Enum.GetName(typeof(RelationOperation), relationOperation), string.Join(", ", objects.Select(x => Enum.GetName(typeof(Runtime), x)))));
		}

		public override Runtime GetProperty(Assembly context, RelationOperation relationOperation = default(RelationOperation)) {
			return _runtimeConverter.ConvertToRuntime(context.ImageRuntimeVersion);
		}

		public override ReferenceAssertitionVerifier ReferenceAssertitionVerifier {
			get { return ReferenceAssertitionVerifier.Runtime; }
		}
		#endregion
	}
}