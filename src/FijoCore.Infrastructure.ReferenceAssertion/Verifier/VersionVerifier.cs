using System;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Module;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Verifier.Base;

namespace FijoCore.Infrastructure.ReferenceAssertion.Verifier {
	public class VersionVerifier : ReferenceAssertitionVerifierBase<RelationOperation, Version[], ulong> {
		private readonly RelativeOperationComparingEnumerableRightService _relativeOperationComparingEnumerableRightService = Kernel.Resolve<RelativeOperationComparingEnumerableRightService>();
		private readonly VersionToULongConverter _versionToULongConverter = Kernel.Resolve<VersionToULongConverter>();

		#region Implementation of IReferenceAssertitionVerifier<RelationOperation,Version[],ulong>
		public override void Verify(Assembly context, RelationOperation relationOperation, Version[] objects) {
			if(!_relativeOperationComparingEnumerableRightService.Compare(relationOperation, GetProperty(context, default(RelationOperation)), objects.Select(_versionToULongConverter.Convert))) throw new InvalidAssemblyException(context, string.Format("AssemblyVersion does not match the assertation. It should be �{0}� �{1}�.", Enum.GetName(typeof(RelationOperation), relationOperation), string.Join(", ", objects.Select(x => x.ToString()))));
		}

		public override ulong GetProperty(Assembly context, RelationOperation relationOperation = default(RelationOperation)) {
			return _versionToULongConverter.Convert(context.GetName().Version);
		}

		public override ReferenceAssertitionVerifier ReferenceAssertitionVerifier {
			get { return ReferenceAssertitionVerifier.Version; }
		}
		#endregion
	}
}