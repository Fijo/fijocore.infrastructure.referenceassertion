﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.ReferenceAssertion.Properties;

namespace FijoCore.Infrastructure.ReferenceAssertionTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new ReferenceAssertionInjectionModule());
		}
	}
}