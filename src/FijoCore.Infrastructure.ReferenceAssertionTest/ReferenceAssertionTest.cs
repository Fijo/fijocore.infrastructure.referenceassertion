﻿using System;
using System.Globalization;
using System.Reflection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Exceptions;
using FijoCore.Infrastructure.ReferenceAssertion.Properties;
using FijoCore.Infrastructure.ReferenceAssertion.Services;
using FijoCore.Infrastructure.ReferenceAssertionTest.Properties;
using FijoCore.Infrastructure.TestTools.FAssert;
using NUnit.Framework;

namespace FijoCore.Infrastructure.ReferenceAssertionTest
{	
	[TestFixture]
	public class ReferenceAssertionTest {
		private ReferenceAssertitionProvider _referenceAssertitionProvider;
		private ReferenceAssertitionVerifierService _referenceAssertitionVerifierService;
		private readonly FAssert _fAssert = new FAssert();

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_referenceAssertitionProvider = Kernel.Resolve<ReferenceAssertitionProvider>();
			_referenceAssertitionVerifierService = Kernel.Resolve<ReferenceAssertitionVerifierService>();
		}
		
		private void TestBase(Action<ReferenceAssertitionProvider> action) {
			_referenceAssertitionProvider.SetContextAssembly(this);
			action(_referenceAssertitionProvider);
			_referenceAssertitionVerifierService.Verify(_referenceAssertitionProvider.GetAssertitions());
		}
		
		[Test]
		public void RuntimeAtLeastDotNet4_0FailTest() {
			_fAssert.Throws<InvalidAssemblyException>(() => 
				TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Runtime, RelationOperation.Less, new[] {Runtime.Net4_0})));
		}

		[Test]
		public void RuntimeAtLeastDotNet2_0Test() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Runtime, RelationOperation.AtLeast, new[]{Runtime.Net2_0}));
		}
		
		[Test]
		public void RuntimeNotInDotNet1_0AndSoOnTest() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Runtime, RelationOperation.NotIn, new[]{Runtime.Net1_0, Runtime.Net1_1}));
		}
		
		[Test]
		public void RuntimeIsInDotNet3_5AndSoOnTest() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Runtime, RelationOperation.IsIn, new[]{Runtime.Net3_5, Runtime.Net4_0, Runtime.Net4_5}));
		}
		
		[Test]
		public void LanguageEqualsInvariantCultureTest() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Language, RelationOperation.Equals, new[]{CultureInfo.InvariantCulture}));
		}
		
		[Test]
		public void VersionAtLeastVersionNullTest() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.Version, RelationOperation.AtLeast, new[]{new Version(0, 0, 0, 0)}));
		}
		
		[Test]
		public void GetExecutingAssembly_VersionAtLeastVersionNullTest() {
			TestBase(provider => provider.Assert(Assembly.GetExecutingAssembly(), ReferenceAssertitionVerifier.Version, RelationOperation.AtLeast, new[]{new Version(0, 0, 0, 0)}));
		}
		
		[Test]
		[Ignore]
		public void ReferenceAssertionInjectionModule_VersionAtLeastVersionNullTest() {
			TestBase(provider => provider.Assert(typeof(ReferenceAssertionInjectionModule), ReferenceAssertitionVerifier.Version, RelationOperation.AtLeast, new[]{new Version(0, 0, 0, 0)}));
		}

		[Test]
		public void DefinedContstantsDebugEnabledTest() {
			TestBase(provider => provider.Assert(ReferenceAssertitionVerifier.DefinedContstants, "DEBUG", true));
		}
		
		[Test]
		[Ignore]
		public void AssemblyDependencyInjection_DefinedContstantsReInjectDisabledTest() {
			TestBase(provider => provider.Assert("FijoCore.Infrastructure.DependencyInjection", ReferenceAssertitionVerifier.DefinedContstants, "ReInject", false));
		}

		[Test]
		[Ignore]
		public void AssemblyDependencyInjection_DefinedContstantsDebugEnabledTest() {
			TestBase(provider => provider.Assert("FijoCore.Infrastructure.DependencyInjection", ReferenceAssertitionVerifier.DefinedContstants, "DEBUG", true));
		}
	}
}
